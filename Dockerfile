FROM python:3.8
RUN mkdir downloads
RUN pip install streamlink
COPY downloads.sh ./downloads.sh
RUN chmod +x ./downloads.sh

ENTRYPOINT ["./downloads.sh"]